# README #

This Project Contains 2 tasks: 

### Challenge 1: Github Stats  ###

It does the following:

* MAKE API request to github repo
* Accepts ORG name and REPO name from stdin
* Outputs name of repo, clone_url, latest_author_name, latest_commit_date in a comma separated as stdout

To maintain the simplicity of project, it is a python script that can be called simply and not an application
    
All the dependencies are listed in requirements.txt

Project contains a Dockerfile. Since it takes input from stdin, do not run it as a daemon.

How to Run?

docker build -t "{{ tag }}" && docker run -it "{{ tag }}"


### Challenge 2: Docker Nginx + Log Management ###

It has following: 

* Nginx Dockerfile with a Welcome Page (nginx dir)
* Fluentd Dockerfile with its configuration file (fluentd dir)
* [Terraform](https://www.terraform.io/intro/index.html) files to create AWS S3 bucket
* Docker Compose file to run nginx and fluentd docker
* Helm charts to deploy nginx and fluentd docker (k8-nginx-helm and k8-fluentd-helm)

Export ACCESS_KEY_ID and SECRET_KEY_ID as Environment Variables. They will be used by terraform and fluentd configuration

These are also used by fluentd helm chart. In K8-fluentd-helm, replace the values under values.yaml by original but ENCRYPTED values.
These will be used to create kubernetes secrets that fluentd helm chart will use.

How to RUN?

* Docker-compose:

"docker-compose up -d" from challenge2 parent folder

* Kubernetes/Helm

" helm upgrade -i nginx k8-nginx-helm" from challenge2 parent folder to run nginx chart

" helm upgrade -i nginx k8-nginx-helm" from challenge2 parent folder to run fluentd chart

NOTE: Access logs files will not be instantly uploaded to s3 due to the configurations settings. Also, if you are running on mac, you might need to make changes in boot2docker to mount host volumes.

To maintain the simplicity of project, docker images used by helm are already pushed to private docker hub registry.
For one click deployment of all the charts, one should also make use of [helmfile](https://github.com/roboll/helmfile) 