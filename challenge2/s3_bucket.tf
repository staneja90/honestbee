resource "aws_s3_bucket" "honestbee-s3" {
  bucket = "${var.bucket_name}"
  region = "${var.aws_region}"
}
