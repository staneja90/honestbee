variable "bucket_name" {
   description = "name of bucket"
   default = "honest-fluentd"
}

variable "region" {
   default = "eu-west-1"
}

variable "aws_access_key" {
   default = ""
   description = "Read from environment variable ACCESS_KEY_ID"
}

variable "aws_secret_key" {
   default = ""
   description = "Read from environment variable SECRET_KEY_ID"
}